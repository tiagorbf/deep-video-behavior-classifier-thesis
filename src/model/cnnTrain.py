from cnnTest import behaviorTest
from keras.utils import np_utils

def train(model, trainSet, batch_size, nb_epoch, output_weights_file, nb_classes, early_stopping):
    X_train = trainSet['netInputs']
    Y_train = trainSet['netLabels']

    Y_train = np_utils.to_categorical(Y_train, nb_classes)

    X_train = X_train.astype("float32")
    X_train /= 255


    print("\n-----Starting the train...\n-----")

    if (early_stopping):
        oldValError = 100000
        for nEpoch in range(0, nb_epoch):
            model.fit(X_train, Y_train, batch_size=batch_size, nb_epoch=1)

            if nEpoch > 0:
                model.save_weights("temp.hdf5", overwrite=True)
                valError = behaviorTest(model, trainSet)

                if valError <= oldValError:
                    model.save_weights(output_weights_file, overwrite=True)
                    print("\n----- Found a better model in epoch " + str(nEpoch) + " -----\n")
                    oldValError = valError
    else:
        model.fit(X_train, Y_train, batch_size=batch_size, nb_epoch=nb_epoch)
        model.save_weights("nets/" + output_weights_file, overwrite=True)
