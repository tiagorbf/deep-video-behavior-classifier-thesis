from model.classifyBags import start_bag_classification
from sh import mv

class bagClassifier(object):
	
	def __init__(self, bags):
		self.bags = bags

	def classify_bags(self):
		for bag in self.bags:
			start_bag_classification("../resources/bags/to_classify/" + bag)
			mv("../resources/bags/to_classify/" + bag + ".bag",
									 "../resources/bags/classified/")

