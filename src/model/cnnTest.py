from cnnPredict import predict
from numpy import array
import numpy as np

numberFramesPerAction = 30

'''
Loads the given net model with the given weights and
tests it against the given dataset.
(Each action contains numberFramesPerAction frames)
and only the global classification of each action
is taken in account
'''


def behaviorTest(model, dataset):
    prediction = 0
    wrong = 0
    count = 0
    nBehaviors = 0
    for i in range(0, len(dataset['netInputs'])):
        prediction += predict(model, inputVal=array([dataset['netInputs'][i]]), batch_size=1)

        if count == numberFramesPerAction - 1:
            average = prediction / 30
            if average > 0.5:
                feedback = 1
            else:
                feedback = 0

            if feedback != dataset['netLabels'][i]:
                wrong += 1

            nBehaviors += 1
            prediction = 0
            count = 0
        else:
            count += 1

    print("-" * 5)
    print("\nError rate is " + str(wrong) + "/" + str(nBehaviors) + " (wrong predictions/total number of actions)\n")
    print("-" * 5)

    return wrong


'''
Loads the given net model with the given weights and
tests it against the given dataset.
(Each action contains numberFramesPerAction frames)
and only the global classification of each action
is taken in account
This function also accept the topError from 1 to 3
'''


def behaviorTestClasses(model, dataset, text_file,test_name):
    wrong = [0,0,0]
    count = 0
    nBehaviors = 0
    class1 = []
    class2 = []
    class3 = []

    X_train = dataset['netInputs'].astype("float16")
    X_train /= 255
    for i in range(0, len(dataset['netInputs'])):
        arr = predict(model, array([X_train[i]]))[0]
        arr = arr.argsort()[-3:][::-1]
        class1.append(arr[0])
        class2.append(arr[1])
        class3.append(arr[2])
        if count == numberFramesPerAction - 1:

            output1 = trend(class1)
            output2 = trend(class2)
            output3 = trend(class3)
            print("CLASSIFICATION: " + str([output1,output2,output3]))
            print("TRUE: " + str(dataset['netLabels'][i][0]))

            wrong = [1 + incrm for incrm in wrong]

            if output1 == dataset['netLabels'][i][0]:
                print("1")
                wrong = [incrm - 1 for incrm in wrong]
            elif output2 == dataset['netLabels'][i][0]:
                print("2")
                wrong[1] -= 1
                wrong[2] -= 1
            elif output3 == dataset['netLabels'][i][0]:
                print("3")
                wrong[2] -= 1

            nBehaviors += 1
            count = 0
            class1 = []
            class2 = []
            class3 = []
        else:
            count += 1


    print("\n -")
    print(wrong)
    print(nBehaviors)
    wrong = [((float(value)/float(nBehaviors)) * 100) for value in wrong]
    print(wrong)
    text_file.write("\n\nError rate of " + test_name)
    text_file.write("\n Top 1 error: " + str(wrong[0]) + "% error rate" )
    text_file.write("\n Top 2 error: " + str(wrong[1]) + "% error rate" )
    text_file.write("\n Top 3 error: " + str(wrong[2]) + "% error rate" )
    text_file.write("\n In " + str(nBehaviors) + " actions")

    return wrong


'''
calculate the most repeated outcome
'''


def trend(classe):
    sortClass = np.copy(np.sort(classe))
    iniValue = sortClass[0]
    newCount = 0
    oldCount = 0
    output = -1
    for it in range(0, len(classe)):
        if (sortClass[it] != iniValue or it == len(classe) - 1):
            if (newCount > oldCount):
                output = sortClass[it - 1]
                oldCount = newCount
            newCount = 0
            iniValue = sortClass[it]
        else:
            newCount += 1
    return output

def most_common(lst):
    return max(set(lst), key=lst.count)