# usage: train -nets structure -lrs -batch_sizes -training test -validation set
from model.cnnTrain import train
from model.netObjects import get_net_structure
import numpy as np
from os import mkdir


class Coach(object):
    def __init__(self, net_names, lrs, batch_sizes, trainingSet, val_set,
                 nb_epoch, output_weights_file, weights_folder):

        self.trainingSet = self.getTrainingSet(trainingSet)
        if weights_folder:
            weights = ["nets/" + weights_folder + "/" + net_name + ".hdf5" for net_name in net_names]
        else:
            weights = [False]

        self.models = get_net_structure(net_names, weights, self.trainingSet['nb_classes'],
                                        len(self.trainingSet['netInputs'][0][0]),
                                        len(self.trainingSet['netInputs'][0][0][0]), False, lrs)
        self.batch_sizes = batch_sizes
        self.val_set = val_set
        self.nb_epoch = nb_epoch
        self.output_weights_file_base = output_weights_file
        self.lrs = lrs
        self.net_names = net_names

    def getTrainingSet(self, trainingSet):
        path_to_dataset = "../resources/datasets/"
        trainSet = {}
        trainSet['netInputs'] = np.load(path_to_dataset + trainingSet + ".npy")
        trainSet['netLabels'] = np.load(path_to_dataset + trainingSet + "_label.npy")
        trainSet['nb_classes'] = self.number_of_classes(trainSet)
        return trainSet

    def getValTest(self, val_set):
        path_to_dataset = "../resources/datasets/"
        val_set = {}
        val_set['netInputs'] = np.load(path_to_dataset + val_set + ".npy")
        val_set['netLabels'] = np.load(path_to_dataset + val_set + "_label.npy")

        return val_set

    def trainNet(self):
        i = 0
        net = 0

        try:
            mkdir("nets/" + self.output_weights_file_base)
        except:
            print("\nFolder Already Exists! Moving on...")
        for model in self.models:
            for batch_size in self.batch_sizes:
                out_file_name = self.output_weights_file_base + "/"
                out_file_name += str(self.net_names[net])
                out_file_name += "_" + self.output_weights_file_base
                out_file_name += "_lr_" + str(self.lrs[i])
                out_file_name += "_bs_" + str(batch_size)
                out_file_name += ".hdf5"

                print("\nInitializing train of net " + out_file_name)

                train(model, self.trainingSet, batch_size, self.nb_epoch,
                      out_file_name, self.trainingSet['nb_classes'], early_stopping=False)
            i += 1
            if i == len(self.lrs):
                i = 0
                net += 1

    def number_of_classes(self, trainingSet):
        n_classes = 1
        classes_founded = [trainingSet['netLabels'][0][0]]
        for i in range(1, len(trainingSet['netLabels'])):
            if trainingSet['netLabels'][i][0] != trainingSet['netLabels'][i - 1][0] \
                    and trainingSet['netLabels'][i][0] not in classes_founded:
                n_classes += 1
                classes_founded.append(trainingSet['netLabels'][i][0])

        return n_classes