'''
Use this file to create a new data set
Usage:
Create new function {name} and change the configurations
'''


def three_type_movements():
    new_dataset = {}

    fast_movements = ['abanar','atirar_cadeira','atirar_objecto', 'correr',
                      'atirar','lutar', 'mexer_os_bracos', 'saltar']

    slow_movements = ['andar', 'arranjar','roubar', 'beber']

    stop_movements = ['parado','sentar', 'sentar_chao','sit_on_table','sit_sink']

    new_dataset['dataset_folders'] = [stop_movements,0] + [slow_movements,1] + [fast_movements,2]
    new_dataset['nSamples'] = 30
    new_dataset['interval'] = 1
    new_dataset['reduceRes'] = 0

    return new_dataset

def four_type_movements():
    new_dataset = {}

    sitted = ['sentar', 'sentar_chao','sit_on_table','sit_sink']

    moving = ['andar','correr','roubar']

    fighting = ['atirar_cadeira','atirar_objecto','atirar','lutar','mexer_os_bracos','saltar','abanar']

    still = ['arranjar', 'beber', 'parado']

    new_dataset['dataset_folders'] = [still,0] + [sitted,1] + [moving,2] + [fighting,3]
    new_dataset['nSamples'] = 30
    new_dataset['interval'] = 1
    new_dataset['reduceRes'] = 0

    return new_dataset