from controller.train import Coach
from controller.predict import Predict
from controller.datasetConfigs import datasetConfigs
from controller.dataset import dataset
from controller.test import tester
from controller.bag import bagClassifier
import argparse
import numpy as np
import os
import sys


def train_factory(netNames, lrs, batchsizes, trainingSet, valSet, nb_epoch, outputWeightsFile, weights):
    net = Coach(netNames, lrs, batchsizes, trainingSet, valSet, nb_epoch, outputWeightsFile, weights)
    net.trainNet()


def test_factory(test_set, weights_folder, type_of_test):
    test = tester(test_set, weights_folder)

    if type_of_test == "binary":
        test.test_net_binary()
    elif type_of_test == "multiple":
        test.test_net_multiple_class()
    else:
        print('-' * 50)
        print("\nPlease specify a type of test: binary or multiple\n")
        print ('-' * 50)


def dataset_factory(dataset_name):
    dataset_configs = getattr(datasetConfigs, dataset_name[0])()

    dataset_obj = dataset(dataset_configs['nSamples'],
                          dataset_configs['interval'],
                          dataset_configs['reduceRes'],
                          dataset_configs['dataset_folders'])

    training_set, validation_set = dataset_obj.create_dataset()
    path_to_dataset_folder = "../resources/datasets/"
    try:
        np.save(path_to_dataset_folder + dataset_name[0] + "_train.npy", training_set['inputs'])
        np.save(path_to_dataset_folder + dataset_name[0] + "_test.npy", validation_set['inputs'])
        np.save(path_to_dataset_folder + dataset_name[0] + "_train_label.npy", training_set['labels'])
        np.save(path_to_dataset_folder + dataset_name[0] + "_test_label.npy", validation_set['labels'])
        print("-" * 50)
        print("Datasets " + str(dataset_name[0]) + " created successfully")
        print("-" * 50)
    except:
        print("-" * 50)
        print("UPS! It was no possible to create datasets " + str(dataset_name[0]))
        print("-" * 50)


def predict_factory(weights_folder,bags):
    predict = Predict(weights_folder,bags)
    predict.classify_bags()


def bags_factory(bags):
    if bags[0]:
        bag = bagClassifier(bags)
        bag.classify_bags()
        print ("-" * 50)
        print ("\nAll your bags were classified\n")
        print ("-" * 50)
    else:
        ls_folder = os.listdir("../resources/bags/to_classify/")
        print ("\nPlease select bag names from this list:\n")
        print (ls_folder)


def main():
    parser = argparse.ArgumentParser(
        description="Offers the possibility to train, predict and create"
                    "a dataset using the keras.io library",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        conflict_handler='resolve')
    parser.add_argument('-m', '--mode',
                        help='What do you pretend to do? '
                             'You can choose: train, test, dataset, predict, bags')
    parser.add_argument('-net', '--net_names',
                        help='A list containing the name of the nets that you want to use'
                             ' (modes: train and predict)', nargs='+')
    parser.add_argument('-lrs', '--learning_rate',
                        help='A list with the lr that you want to train your net with '
                             '(mode: train)', type=float, nargs='+')
    parser.add_argument('-b', '--batchsizes',
                        help='A list of bachsizes you want to train your net with '
                             '(mode: train)', type=int, nargs='+')
    parser.add_argument('-nepochs', '--nbEpochs',
                        help='Specify the number of epochs that you want to train the net '
                             '(mode: train)', type=int)
    parser.add_argument('-out', '--outFile',
                        help='File where you want to save the output weights '
                             '(mode: train)')
    parser.add_argument('-trS', '--trainingSet',
                        help='Name of the training set you want to use '
                             '(mode: train')
    parser.add_argument('-inS', '--input_set',
                        help='Name of the validation set you want to use '
                             '(modes: train (validation), test and predict)')
    parser.add_argument('-dataset', '--dataset', help='Name of the dataset that you want to use'
                                                      '(configure it in datasetConfigs/datasetConfigs.py) '
                                                      '(applicable in the mode: dataset)', nargs='+')
    parser.add_argument('-wf', '--weights_folder',
                        help='Name of the file containing the weights to initialize the net '
                             '(modes: test and predict)'
                        )
    parser.add_argument('-bags', '--bags', help='Name of the bags that you want to '
                                                'classify (mode: bags, predict)', nargs='+',
                        default=[False])
    parser.add_argument('-tpTest', '--type_of_test', help='Choose between binary of multiple test (mode : test)',
                        default="multiple")

    args = parser.parse_args()

    if args.mode == "train":

        if not args.net_names or not args.learning_rate or not args.batchsizes \
                or not args.trainingSet or not args.nbEpochs \
                or not args.outFile:

            print("\nArguments: \n-net (net names) -lrs (learning rates) -b (btach sizes) -nepochs"
                  " -out (output file name) -trS (training set) -wf (optional, name of folder to initiate net, "
                  "the weigths file name must have the net name)")
        else:
            train_factory(args.net_names, args.learning_rate, args.batchsizes,
                          args.trainingSet, args.input_set, args.nbEpochs, args.outFile, args.weights_folder)

    elif args.mode == "test":
        if not args.input_set or not args.weights_folder:
            print("\nArguments: \n-inS (input dataset to test) "
                  "-wf (name of folder to initiate net, the weigths file name must have the net name), "
                  "-tpTest (optional, if the test is binary or multiple classes)")
        else:
            test_factory(args.input_set, args.weights_folder, args.type_of_test)

    elif args.mode == "dataset":
        if not args.dataset:
            print("\nArguments: \n -dataset (name of the dataset that you want to create, "
                  "must be defined in controller/datasetConfigs)")
        else:
            dataset_factory(args.dataset)

    elif args.mode == "predict":
        if not args.bags or not args.weights_folder:
            print("\nArguments: \n-inS (input dataset to test) "
                  "-wf (name of folder to initiate net, the weigths file name must have the net name), "
                  "-bags (name of bags to predict)")
        else:
            predict_factory(args.weights_folder, args.bags)

    elif args.mode == "bags":
        bags_factory(args.bags)

    else:
        print ("-" * 50)
        print ("\nCan not recognize mode!\n\nPlease select: train, test, dataset, predict or bags\n")
        print ("-" * 50)
        sys.exit(-1)


if __name__ == '__main__':
    main()
