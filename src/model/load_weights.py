from __future__ import absolute_import
from __future__ import print_function
from six.moves import range


# the function load_weights has been altered so that the user can choose ih he wants to
#  inicialize all layers or just n layers

def load_weights_2(model, filepath, n=0):
    # Loads weights from HDF5 file
    import h5py
    f = h5py.File(filepath)
    if n > 0:  # load weights until layer n
        nlayers = n
        k = 0
    elif n < 0:  # load weights from layer n
        k = -n
        nlayers = f.attrs['nb_layers']
    else:  # load all weigths
        nlayers = f.attrs['nb_layers']
        k = 0
    thisLayer = 0
    # k is the starting layer of the hdf5 file
    while k < nlayers:
        g = f['layer_{}'.format(k)]
        weights = [g['param_{}'.format(p)] for p in range(g.attrs['nb_params'])]
        model.layers[thisLayer].set_weights(weights)
        thisLayer = thisLayer + 1
        k = k + 1
    f.close()
    return model
