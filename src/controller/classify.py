#usage: classify -net -dataset
from model.cnnPredict import predict
from model.netObjects import get_net_structure

class predictor(object):

    def __init__(self, net_names, weights):
        self.model = get_net_structure(net_names=net_names, weights=weights)
        self.weights = weights

    def classify(self,inputVal):
        prediction = predict(self.model,self.weights,inputVal, batch_size=1)
        return prediction