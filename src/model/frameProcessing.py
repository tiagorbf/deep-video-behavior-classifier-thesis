import numpy as np
from numpy import array
from PIL import Image
from os import listdir, mkdir
from sys import exit



# this function receivs a numpy array and reduces it resolution
def reduceResolution(narray, reduceRes, nChannels=3):
    if len(narray.shape) == 3:  # only for single images
        narray = np.array([narray])

    newArr = []
    for i in range(0, len(narray)):
        imgArr = []
        for channel in range(0, nChannels):
            newLine = []
            for line in range(0, len(narray[i][0]), reduceRes):
                newLine.append(narray[i][channel][line][0:len(narray[i][channel][0]):reduceRes])
            imgArr.append(newLine)
        newArr.append(imgArr)
    return array(newArr)


# this function eliminates the header of the image
def removeHeader(narray, size=5):
    if len(narray.shape) == 3:  # only for single images
        narray = np.array([narray])
    newArr = []
    for i in range(0, len(narray)):
        imgArr = []
        for channel in range(0, 3):
            newLine = []
            for line in range(size, len(narray[i][0])):
                newLine.append(narray[i][channel][line][0:len(narray[i][channel][0])])
            imgArr.append(newLine)
        newArr.append(imgArr)
    if len(narray.shape) == 3:
        return array(newArr[0])
    else:
        return array(newArr)


# this functions converts the narray to the image format: [npixeis,npixeis,nchannels]
def convert_dataset_to_video(narray, reduceRes, nChannels):
    if reduceRes > 0:
        narray = reduceResolution(narray, reduceRes, nChannels)
        narray = narray[0]
    lineArr = []
    for line in range(0, len(narray[0])):  # iterates in the lines
        colArr = []
        for col in range(0, len(narray[0][0])):  # iterates in the column
            pixel = [narray[2][line][col], narray[1][line][col], narray[0][line][col]]  # RGB pixel
            colArr.append(pixel)
        lineArr.append(colArr)
    imgArr = array(lineArr)
    return imgArr


# Converts an array into an image
# if nSamples>0 then only care about the n last frames
# if Interval>0 then b frames between each frame
# if reduceRes>0 than it reduces the image in reduceRes scale
def seeImage(dataset, nSamples, Interval, reduceRes, mode, nChannels=3):

    image = []

    if nSamples > 0 and len(dataset) > nSamples:
        dataset = dataset[len(dataset) - nSamples:len(dataset):Interval]

    for i in range(0, len(dataset)):
        imgArr = convert_dataset_to_video(dataset[i], reduceRes, nChannels)
        img = Image.fromarray(imgArr, 'RGB')
        i = 0
        if mode == "save":
            fileName = "tmp/" + str(i) + ".png"
            img.save(fileName)
            i += 1
        else:
            print("\nMode not recognise")
            exit(-1)


    return image

def create_tmp_folder():
    folders = listdir(".")
    if "tmp" not in folders:
        print("\nCreate folder tmp...")
        mkdir("tmp")
    else:
        print("Folder tmp already exists! Moving on...")



# remove the background from the image by deriving in time
def removeBackground(narray):
    narray = narray.astype(np.int16)
    narray_aux = np.copy(narray)
    for i in range(0, len(narray)):
        if i < len(narray) - 1:
            narray_aux[i] = narray[i] - narray[i + 1]
        else:
            narray_aux[i] = narray[i] - narray[i - 1]

    narray = narray_aux.clip(min=0)
    narray = narray.astype(np.uint8)

    return narray


def convert_video_to_dataset(rawimg):
    newimg = []
    newimgaux = []
    newimgR = []
    newimgG = []
    newimgB = []
    for lines in range(0, len(rawimg)):  # lines of rawimg
        imglinesR = []
        imglinesG = []
        imglinesB = []
        for col in range(0, len(rawimg[1])):  # columns of rawimg
            imglinesR.append(rawimg[lines][col][0])  # R intensity
            imglinesG.append(rawimg[lines][col][1])  # G intensity #G intensity
            imglinesB.append(rawimg[lines][col][2])  # B intensity #B intensity

        newimgR.append(imglinesR)
        newimgG.append(imglinesG)
        newimgB.append(imglinesB)

    newimgaux.append(newimgR)
    newimgaux.append(newimgG)
    newimgaux.append(newimgB)
    newimg.append(newimgaux)  # [nimages,nchannels,npixeis,npixeis]

    return array(newimg)
