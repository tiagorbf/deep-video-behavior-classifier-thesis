import numpy as np
from model.netObjects import get_net_structure
from model.frameProcessing import seeImage, create_tmp_folder
from os import listdir
from model.cnnPredict import start_bag_classification
from sh import mv

class Predict(object):

        def __init__(self, weights_folder, bags):
            self.weights_folder = weights_folder
            path_to_weights = "nets/"
            model_names = listdir(path_to_weights + weights_folder)
            net_names = []
            for model_name in model_names:
                net_names.append(model_name.split("_")[0])

            self.net = get_net_structure(net_names=net_names,
                                        weights=[path_to_weights + weights_folder + "/" + weight_name for weight_name in model_names],
                                        nb_classes=int(weights_folder.split("_")[0]),
                                        h=110,
                                        w=160, test=True)
            self.bags = bags

        def getTrainingSet(self,input_set):
            path_to_dataset = "../resources/datasets/"
            trainSet = {}
            trainSet['netInputs'] = np.load(path_to_dataset + input_set + ".npy")
            trainSet['netLabels'] = np.load(path_to_dataset + input_set + "_label.npy")
            trainSet['nb_classes'] = self.number_of_classes(trainSet)
            return trainSet

        def number_of_classes(self,trainingSet):
            n_classes = 1
            classes_founded = [trainingSet['netLabels'][0][0]]
            for i in range(1,len(trainingSet['netLabels'])):
                if trainingSet['netLabels'][i][0] != trainingSet['netLabels'][i-1][0] \
                        and trainingSet['netLabels'][i][0] not in classes_founded:
                    n_classes += 1
                    classes_founded.append(trainingSet['netLabels'][i][0])

            return n_classes


        def save_image(self):
            create_tmp_folder()
            seeImage(self.input_set['netInputs'], nSamples=0, Interval=1, reduceRes=0, mode="save", nChannels=3)



        def classify_bags(self):
            for bag in self.bags:
                start_bag_classification("../resources/bags/classified/" + bag, self.net[0])