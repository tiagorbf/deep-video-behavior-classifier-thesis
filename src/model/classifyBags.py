import os
#import rospy
import numpy as np
#import cv2

#from sh import roscore, rosrun, rosbag, mv

#from subprocess import check_output

#from sensor_msgs.msg import CompressedImage

from model.frameProcessing import reduceResolution
from model.frameProcessing import removeHeader
from model.frameProcessing import convert_video_to_dataset

frame_rate = 10
frame = frame_rate
folders = []
saved_frames = []
stop_flag = 0
counter = 0


def start_bag_classification(name_of_bag_file):
    create_host_foler()
    launch_roscore()
    launch_video_viwer()
    launch_rosbag(name_of_bag_file)
    listen_bag()


def launch_roscore():
    result = check_output(" ps -ef | grep /opt/ros/indigo/bin/roscore | grep -v grep | wc -l", shell=True)
    if int(result) == 0:
        print ("\nInitializing Roscore...\n")
        roscore(_bg=True)
    else:
        print("\nRoscore is already running. Moving on...\n")


def launch_rosbag(name_of_bag_file):
    print("\nStarting rosbag " + name_of_bag_file + "\n")
    rosbag.play(name_of_bag_file + ".bag",_bg=True)


def launch_video_viwer():
    result = check_output(" ps -ef | grep image_view | grep -v grep | wc -l", shell=True)
    if int(result) == 0:
        print("\nInitializing video...\n")
        rosrun("image_view", "image_view", "image:=/axis/image_raw", "_image_transport:=compressed", _bg=True)

def listen_bag():
    global stop_flag
    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber("/axis/image_raw/compressed", CompressedImage, callback)

    print("\nStarting Classifier\n")
    while 1:

        stop = raw_input()

        if stop == "":
            global savedX
            global savedY
            global counter
            stop_flag = 1

            print("-" * 50)
            label = raw_input("\nClassify the image:\n")
            print("-" * 50)

            path_to_folder = create_new_labeled_folder(label)
            create_new_labeled_file(path_to_folder, label)

            stop_flag = 0
            counter = 0

        elif stop == "q":
            print("\n-----Finishing Program------\n")
            return
        else:
            print("\n-----Unknown Key-----\nPress:\n enter - classify\n q - exit program\n")


def create_new_labeled_file(path_to_label_folder, label):
    ls_folder = os.listdir(path_to_label_folder)
    file_name = path_to_label_folder + "/" + label + str(len(ls_folder))
    np.save(file_name, saved_frames)


def create_new_labeled_folder(label):
    global folders

    path_to_labeled_folder = "classifications/" + label
    if label not in folders:
        try:
            os.mkdir(path_to_labeled_folder)
        except:
            print("\nFolder " + label + " already exists. Moving on...")

        folders.append(label)

    return path_to_labeled_folder


def create_host_foler():
    try:
        os.mkdir("classifications")
    except:
        print("\nFolder classifications already exits. Moving on...\n")


def callback(image_data):
    global saved_frames
    global frame
    global counter
    if stop_flag == 0:
        if frame == frame_rate:
            image = conver_img_to_numpy(image_data)

            if len(saved_frames) == 0:
                saved_frames = image
            else:
                saved_frames = np.concatenate((saved_frames, image))
                if len(saved_frames) > 30:
                    saved_frames = saved_frames[1:len(saved_frames)]
            frame = 0
            counter += 1
            print(counter)
        else:
            frame += 1


def conver_img_to_numpy(image_data):
    np_arr = np.fromstring(image_data.data,
                           np.uint8)  # the structure is lines(pxs)xcolumms(pxs)xchanels(rgb) (240x320x3)
    image_np = cv2.imdecode(np_arr, cv2.CV_LOAD_IMAGE_COLOR)
    image = convert_video_to_dataset(image_np)
    image = reduceResolution(image[0], 2)
    image = removeHeader(image, size=10)

    return image
