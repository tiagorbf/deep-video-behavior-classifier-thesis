import numpy as np
from model.frameProcessing import reduceResolution
from os import listdir
from sys import exit
from sh import pwd
from numpy import array, concatenate
'''
#Function that loads the given datasets and returns all in one narray
#names - paths to the narray
#nSamples - number of samples to take from the array
#if nSamples=0 then use all the narray
#interval- interval between frames
'''
def load_datasets(desired_dataset,nSamples,interval,reduceRes,nChannels=3):
    dataset = np.array([])
    dataset_labels = np.array([])
    final_dataset = {}
    path_to_classification_folder = "classifications/"

    for class_n in range(0,len(desired_dataset),2):
        for folder_name in desired_dataset[class_n]:

            names_in_folder = listdir(path_to_classification_folder + folder_name)

            for i in range(0, len(names_in_folder)):
                loadfile = np.load(path_to_classification_folder + folder_name
                                   + "/" + names_in_folder[i])

                #selects last nSamples of the array
                if nSamples > 0 and len(loadfile) > nSamples:
                    loadfile = loadfile[len(loadfile)-nSamples:len(loadfile)]

                if reduceRes > 0:
                    loadfile = reduceResolution(loadfile,reduceRes,nChannels)

                loadfile = loadfile[0:len(loadfile):interval]

                if len(dataset) == 0:
                    dataset = loadfile
                    dataset_labels = [[desired_dataset[class_n + 1]]] * len(loadfile)
                else:
                    dataset = np.concatenate((dataset,np.array(loadfile)))
                    dataset_labels = np.concatenate((dataset_labels,np.array([[desired_dataset[class_n + 1]]] * len(loadfile))))

    final_dataset['inputs'] = dataset
    final_dataset['labels'] = dataset_labels

    return final_dataset

def split_dataset(dataset):
    training_set = []
    validation_set = array([])
    training_set_labels = []
    validation_set_labels = array([])
    count = 0
    i = 0
    while i < len(dataset['inputs']):
        if count == 60:
            if len(validation_set) == 0:
                validation_set = dataset['inputs'][i:(i+30)]
                validation_set_labels = dataset['labels'][i:(i+30)]
            else:
                validation_set = concatenate((validation_set,dataset['inputs'][i:(i+30)]))
                validation_set_labels = concatenate((validation_set_labels,dataset['labels'][i:(i+30)]))
            i += 30
            count = 0
        else:
            training_set.append(dataset['inputs'][i])
            training_set_labels.append(dataset['labels'][i])
            count += 1
            i += 1

    final_tr_set = {}
    final_val_set = {}
    final_tr_set['inputs'] = training_set
    final_tr_set['labels'] = training_set_labels
    final_val_set['inputs'] = array(validation_set)
    final_val_set['labels'] = array(validation_set_labels)

    return final_tr_set, final_val_set
