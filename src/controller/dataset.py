from model.cnndataset import load_datasets, split_dataset
from numpy import save, load

class dataset(object):

    def __init__(self, nSamples, interval, reduceRes ,inputBagPaths):
        self.nSamples = nSamples
        self.interval = interval
        self.reduceRes = reduceRes
        self.inputBagPaths = inputBagPaths

    #dataset['netInputs'] - [nimages,nchannels,npixeis,npixeis]
    #dataset['netLabels'] - [n_classificacoes_images]
    def create_dataset(self):

        full_dataset = load_datasets(self.inputBagPaths,self.nSamples,self.interval,self.reduceRes)
        return split_dataset(full_dataset)
