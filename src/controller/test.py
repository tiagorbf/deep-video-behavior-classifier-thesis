#usage should be: test.py -net names -test sets
from model.cnnTest import behaviorTest
from model.cnnTest import behaviorTestClasses
from model.netObjects import get_net_structure
from os import listdir
import numpy as np
class tester(object):

    def __init__(self, dataset,weights_folder):
        path_to_weights = "nets/"
        self.dataset = self.getTrainingSet(dataset)
        self.weights_folder = weights_folder
        self.models_to_test = listdir(path_to_weights + "/" + weights_folder)
        net_names = []
        for model_name in self.models_to_test:
            net_names.append(model_name.split("_")[0])

        self.models = get_net_structure(net_names=net_names,
                                        weights=[path_to_weights + weights_folder + "/" + weight_name for weight_name in self.models_to_test],
                                        nb_classes=self.dataset['nb_classes'],
                                        h=len(self.dataset['netInputs'][0][0]),
                                        w=len(self.dataset['netInputs'][0][0][0]), test=True)


    def test_net_binary(self):
        for model in self.models:
            behaviorTest(model,self.dataset)

    def test_net_multiple_class(self):
        test_file = open(self.weights_folder + "_test.txt", "a")
        i = 0
        for model in self.models:
            print("Testing " + self.models_to_test[i])
            behaviorTestClasses(model,self.dataset,test_file,self.models_to_test[i])
            i += 1

    def getTrainingSet(self,trainingSet):
        path_to_dataset = "../resources/datasets/"
        trainSet = {}
        trainSet['netInputs'] = np.load(path_to_dataset + trainingSet + ".npy")
        trainSet['netLabels'] = np.load(path_to_dataset + trainingSet + "_label.npy")
        trainSet['nb_classes'] = self.number_of_classes(trainSet)
        return trainSet

    def number_of_classes(self,trainingSet):
        n_classes = 1
        classes_founded = [trainingSet['netLabels'][0][0]]
        for i in range(1,len(trainingSet['netLabels'])):
            if trainingSet['netLabels'][i][0] != trainingSet['netLabels'][i-1][0] \
                    and trainingSet['netLabels'][i][0] not in classes_founded:
                n_classes += 1
                classes_founded.append(trainingSet['netLabels'][i][0])

        return n_classes