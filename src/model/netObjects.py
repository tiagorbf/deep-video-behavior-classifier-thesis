from keras.optimizers import SGD
from keras.layers.core import Dense, Activation, Flatten, Dropout
from keras.layers.convolutional import MaxPooling2D, Convolution2D
from keras.models import Sequential
from load_weights import load_weights_2

import sys

def get_net_structure(net_names, weights, nb_classes, h, w, test, learning_rates = [1]):
    net = []
    i = 0
    for net_name in net_names:
        for learning_rate in learning_rates:
            print("\nCreating net " + net_name)
            if weights[0] != False:
                print("Weight: " + weights[i])
            new_model = getattr(sys.modules[__name__],net_name)(learning_rate, weights[i], nb_classes, h, w, test)
            net.append(new_model)
        if weights[0] != False:
            i += 1
    return net

    # all net structers must be specified here.


def smallNet(learningR, weights, nb_classes,h,w, test):
    model = Sequential()

    model.add(
        Convolution2D(1, 3, 3, border_mode='valid', input_shape=(3, h, w)))  # (n kernels, n channels, lines of kernel, cols of kernel)
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))  # reduces x/a,y/b (a,b are the arguments) the dimensions of the image
    model.add(Dropout(0.75))

    model.add(Flatten())
    model.add(Dense(100, init='lecun_uniform'))  # (in units, out units)
    model.add(Activation('relu'))
    model.add(Dropout(0.75))

    model.add(Dense(nb_classes, init='lecun_uniform'))
    model.add(Activation('tanh'))

    sgd = SGD(lr=learningR, decay=1e-6, momentum=0.05, nesterov=True)
    model.compile(loss='mean_squared_error', optimizer=sgd)

    model = load_weights_net(weights,model,test,4)

    return model


def bigNet(learningR, weights, nb_classes, h, w , test):
    model = Sequential()

    model.add(Convolution2D(1, 9, 9, border_mode='valid', input_shape=(3, h, w)))  # (n kernels, n channels, lines of kernel, cols of kernel)
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))  # reduces x/a,y/b (a,b are the arguments) the dimensions of the image

    model.add(Convolution2D(3, 6, 6))  # (n kernels, n channels, lines of kernel, cols of kernel)
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))  # reduces x/a,y/b (a,b are the arguments) the dimensions of the image

    model.add(Convolution2D(6, 3, 3))  # (n kernels, n channels, lines of kernel, cols of kernel)
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))  # reduces x/a,y/b (a,b are the arguments) the dimensions of the image

    model.add(Convolution2D(9, 3, 3))  # (n kernels, n channels, lines of kernel, cols of kernel)
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))  # reduces x/a,y/b (a,b are the arguments) the dimensions of the image

    model.add(Convolution2D(12, 3, 3))  # (n kernels, n channels, lines of kernel, cols of kernel)
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))  # reduces x/a,y/b (a,b are the arguments) the dimensions of the image
    model.add(Dropout(0.5))

    model.add(Flatten())
    model.add(Dense(100, init='lecun_uniform'))  # (in units, out units)
    model.add(Activation('relu'))
    model.add(Dropout(0.25))

    model.add(Dense(nb_classes, init='lecun_uniform'))
    model.add(Activation('softmax'))
    # let's train the model using SGD + momentum (how original).
    sgd = SGD(lr=learningR, decay=1e-6, momentum=0.5, nesterov=True)
    model.compile(loss='mean_squared_error', optimizer=sgd)

    model = load_weights_net(weights,model,test,16)

    return model

def bigNetPlus(learningR, weights, nb_classes, h, w , test):
    model = Sequential()

    model.add(Convolution2D(2, 9, 9, border_mode='valid', input_shape=(3, h, w)))  # (n kernels, n channels, lines of kernel, cols of kernel)
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))  # reduces x/a,y/b (a,b are the arguments) the dimensions of the image

    model.add(Convolution2D(3, 6, 6))  # (n kernels, n channels, lines of kernel, cols of kernel)
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))  # reduces x/a,y/b (a,b are the arguments) the dimensions of the image

    model.add(Convolution2D(6, 3, 3))  # (n kernels, n channels, lines of kernel, cols of kernel)
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))  # reduces x/a,y/b (a,b are the arguments) the dimensions of the image

    model.add(Convolution2D(9, 3, 3))  # (n kernels, n channels, lines of kernel, cols of kernel)
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))  # reduces x/a,y/b (a,b are the arguments) the dimensions of the image

    model.add(Convolution2D(12, 3, 3))  # (n kernels, n channels, lines of kernel, cols of kernel)
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))  # reduces x/a,y/b (a,b are the arguments) the dimensions of the image
    model.add(Dropout(0.5))

    model.add(Flatten())
    model.add(Dense(100, init='lecun_uniform'))  # (in units, out units)
    model.add(Activation('relu'))
    model.add(Dropout(0.5))

    model.add(Dense(nb_classes, init='lecun_uniform'))
    model.add(Activation('softmax'))
    # let's train the model using SGD + momentum (how original).
    sgd = SGD(lr=learningR, decay=1e-6, momentum=0.5, nesterov=True)
    model.compile(loss='mean_squared_error', optimizer=sgd)

    model = load_weights_net(weights,model,test,16)

    return model

def load_weights_net(weights,model,test,n):

    if weights:
        print("Loading Weights: " + weights)
        if test:
            model.load_weights(weights)
        else:
            model = load_weights_2(model,weights,n)

    return model